﻿using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GenCan;
using GenCan.GUI;
using Gr.Net.MaroulisLib;
using NextGenCanMobile;

namespace App1
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            // Show SplashScreen
            var config = new EasySplashScreen(this)
                .WithFullScreen()
                .WithTargetActivity(Java.Lang.Class.FromType(typeof(Navigation)))
                .WithSplashTimeOut(3000) // 3 sec duration
                .WithBackgroundColor(Color.ParseColor("#ffffff"))
                .WithLogo(Resource.Drawable.gclogo2)
                .WithHeaderText("Welcome")
                .WithFooterText("Copyright 2019-2020")
                .WithBeforeLogoText("Next Gen")
                .WithAfterLogoText("Gen Can");

            //Set Text Color
            config.HeaderTextView.SetTextColor(Color.DarkGray);
            config.FooterTextView.SetTextColor(Color.DarkGray);
            config.BeforeLogoTextView.SetTextColor(Color.DarkGray);
            config.AfterLogoTextView.SetTextColor(Color.DarkGray);

            // Create View
            View view = config.Create();

            // SetContentView
            SetContentView(view);


        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}